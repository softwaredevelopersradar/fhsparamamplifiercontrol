﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace FPSFullStateControl
{
    public class FullParam
    {
        private short powerState;
        private float voltage1State;
        private float voltage2State;
        private short temp1State;
        private short temp2State;
        private short temp3State;
        private short temp4State;
        private float cur1State;
        private float cur2State;
        private float cur3State;
        private float cur4State;
        public int ErrorCodO1 { get; set; }//расшифр код ошибки синтезаторов
        public int ErrorCodO2 { get; set; }  //расшифр код ошибки УМ


        public short PowerState
        {
            get { return powerState; }
            set
            {
                if ((value < 2 || value > 150) && value!=0)
                { powFlag = true; }
                else powFlag = false;
                powerState = value;
            }
        }
        public float Voltage1State
        {
            get { return voltage1State; }
            set
            {
                if (value < 0 || value > 230)
                { volFlag[0] = true; }
                else volFlag[0] = false;
                voltage1State = value;
            }
        }
        public float Voltage2State
        {
            get { return voltage2State; }
            set
            {
                if (value < 0 || value > 230)
                { volFlag[1] = true; }
                else volFlag[1] = false;
                voltage2State = value;
            }
        }

        public float Cur1State
        {
            get
            {  return cur1State; }
            set
            {
                if (value < 0 || value > 32)
                    { curFlag[0] = true; }
                else curFlag[0] = false;
                cur1State = value;
            }
        }
        public float Cur2State
        {
            get
            { return cur2State; }
            set
            {
                if (value < 0 || value > 32)
                    { curFlag[1] = true; }
                else curFlag[1] = false;
                cur2State = value;
            }
        }
        public float Cur3State
        {
            get
            { return cur3State; }
            set
            {
                if (value < 0 || value > 32)
                { curFlag[2] = true; }
                else curFlag[2] = false;
                cur3State = value;
            }
        }
        public float Cur4State
        {
            get
            { return cur4State; }
            set
            {
                if (value < 0 || value > 32)
                { curFlag[3] = true; }
                else curFlag[3] = false;
                cur4State = value;
            }
        }

        public short Temp1State
        {
            get
            { return temp1State; }
            set
            {
                int sign = value & 128;
                int state = value&127;
                temp1State = Convert.ToInt16(Math.Pow(-1, sign) * state);
                if (temp1State < -50 || temp1State > 100)
                    { tempFlag[0] = true; }
                else tempFlag[0] = false;
            }
        }
        public short Temp2State
        {
            get
            { return temp2State; }
            set
            {
                int sign = value & 128;
                int state = value & 127;
                temp2State = Convert.ToInt16(Math.Pow(-1, sign) * state);
                if (temp2State < -50 || temp2State > 100)
                { tempFlag[1] = true; }
                else tempFlag[1] = false;
            }
        }
        public short Temp3State
        {
            get
            { return temp3State; }
            set
            {
                int sign = value & 128;
                int state = value & 127;
                temp3State = Convert.ToInt16(Math.Pow(-1, sign) * state);
                if (temp3State < -50 || temp3State > 100)
                { tempFlag[2] = true; }
                else tempFlag[2] = false;
            }
        }
        public short Temp4State
        {
            get
            { return temp4State; }
            set
            {
                int sign = value & 128;
                int state = value & 127;
                temp4State = Convert.ToInt16(Math.Pow(-1, sign) * state);
                if (temp4State < -50 || temp4State > 100)
                { tempFlag[3] = true; }
                else tempFlag[3] = false;
            }
        }

        public int letter { get; set; } = 0;

        public bool[] volFlag { get; set; } = new bool[2];
        public bool powFlag { get; set; }
        public bool[] tempFlag { get; set; } = new bool[4];
        public bool[] curFlag { get; set; } = new bool[4];

        public FullParam()
        {
        }
        
    }
}