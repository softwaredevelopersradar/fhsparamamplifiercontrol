﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FPSFullStateControl
{

    public class GlobalState : INotifyPropertyChanged
    {

        public ObservableCollection<FullParam> FullParams { get; set; }
        public ObservableCollection<FullParam> TempCollection { get; set; }


        public GlobalState()
        {
            TempCollection = new ObservableCollection<FullParam> { };
            FullParams = new ObservableCollection<FullParam> { };
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

    }
}