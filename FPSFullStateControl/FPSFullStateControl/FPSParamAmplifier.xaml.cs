﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System;

namespace FPSFullStateControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class FPSParamAmplifier : UserControl
    {
        public int numOfLetters { get; set; } = 10;

        public FPSParamAmplifier()
        {
            InitializeComponent();
            paramAmplifire.DataContext = new GlobalState();
            paramAmplifire.ItemsSource = (paramAmplifire.DataContext as GlobalState).FullParams;
            AddEmptyRows();
        }

        /// <summary>
        /// Обновить значение ошибок (типа передали 3 байта ошибки)
        /// </summary>
        public List<int> UpdateError(byte[] errors, int litercount)
        {
            var forQuickControl = new List<int>();
            try
            {
                var _errorCodO1 = new int[litercount];
                var _errorCodO2 = new int[litercount];
                _errorCodO1 = FillError(_errorCodO1, errors[1]);
                _errorCodO2 = FillError(_errorCodO2, errors[2]);

                ((GlobalState)paramAmplifire.DataContext).TempCollection = new ObservableCollection<FullParam>(((GlobalState)paramAmplifire.DataContext).FullParams);
                for (int i = 0; i < litercount; i++)
                {
                    ((GlobalState)paramAmplifire.DataContext).FullParams.RemoveAt(i);
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].ErrorCodO1 = _errorCodO1[i];
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].ErrorCodO2 = _errorCodO2[i];
                    ((GlobalState)paramAmplifire.DataContext).FullParams.Insert(i, ((GlobalState)paramAmplifire.DataContext).TempCollection[i]);
                }
                
                for (int i = 0; i < litercount; i++)
                {
                    if (_errorCodO2[i] == 1) forQuickControl.Add(i); 
                }
                return forQuickControl;
            }
            catch { return forQuickControl; }
        }

        private int[] FillError(int[] decodedErrors, byte recivedError)
        {
            decodedErrors[0] = decodedErrors[2] = recivedError & 1;
            decodedErrors[1] = decodedErrors[3] = (recivedError >> 1) & 1;
            for (int i = 2; i < numOfLetters - 2; i++)
            {
                decodedErrors[i + 2] = (recivedError >> i) & 1;
            }
            return decodedErrors;
        }

        /// <summary>
        /// Обновить значение мощности
        /// </summary>
        public void UpdatePower(short[] power, short literCount)
        {
            try
            {
                ((GlobalState)paramAmplifire.DataContext).TempCollection = new ObservableCollection<FullParam>(((GlobalState)paramAmplifire.DataContext).FullParams);
                for (int i = 0; i < power.Length; i ++)
                {
                    ((GlobalState)paramAmplifire.DataContext).FullParams.RemoveAt(i);
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].PowerState =power[i];
                    ((GlobalState)paramAmplifire.DataContext).FullParams.Insert(i, ((GlobalState)paramAmplifire.DataContext).TempCollection[i]);
                }
            }
            catch { }
        }
        /// <summary>
        /// Обновить значение напряжения
        /// </summary>
        public void UpdateVoltage(float[] voltage, short literCount)
        {
            try
            {
                //разбиваем voltage на 2 разных бита
                float[] newVolt = new float[literCount * 2];
                for (int i = 0; i < literCount; i++)
                {
                    int _volt = Convert.ToInt32(voltage[i]);
                    newVolt[i * 2] = Convert.ToSingle((byte)(_volt & 0xff));
                    newVolt[i * 2 + 1] = Convert.ToSingle((byte)((_volt >> 8) & 0xff));
                }
                ((GlobalState)paramAmplifire.DataContext).TempCollection = new ObservableCollection<FullParam>(((GlobalState)paramAmplifire.DataContext).FullParams);
                for (int i = 0; i < literCount; i++)
                {
                    
                    //((GlobalState)paramAmplifire.DataContext).FullParams.RemoveAt(i);
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Voltage1State = newVolt[i*2];
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Voltage2State = newVolt[i*2+1];
                    ((GlobalState)paramAmplifire.DataContext).FullParams.RemoveAt(i);
                    ((GlobalState)paramAmplifire.DataContext).FullParams.Insert(i, ((GlobalState)paramAmplifire.DataContext).TempCollection[i]);
                }
            }
            catch { }
        }
        /// <summary>
        /// Обновить значение тока
        /// </summary>
        public void UpdateCurrent(byte[] current, int literCount)
        {
            try
            {
                var _newCurr = new float[current.Length - literCount];
                for (int i = 0; i < literCount; i++)
                {
                    _newCurr[i * 4] = Convert.ToSingle(current[i * 5 + 1]);
                    _newCurr[i * 4 + 1] = Convert.ToSingle(current[i * 5 + 2]);
                    _newCurr[i * 4 + 2] = Convert.ToSingle(current[i * 5 + 3]);
                    _newCurr[i * 4 + 3] = Convert.ToSingle(current[i * 5 + 4]);
                }
                ((GlobalState)paramAmplifire.DataContext).TempCollection = new ObservableCollection<FullParam>(((GlobalState)paramAmplifire.DataContext).FullParams);
                for (int i = 0; i < literCount; i++)
                {
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Cur1State = _newCurr[i*4];
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Cur2State = _newCurr[i*4+1];
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Cur3State = _newCurr[i*4+2];
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Cur4State = _newCurr[i*4+3];

                    ((GlobalState)paramAmplifire.DataContext).FullParams.RemoveAt(i);
                    ((GlobalState)paramAmplifire.DataContext).FullParams.Insert(i, ((GlobalState)paramAmplifire.DataContext).TempCollection[i]);
                }
            }
            catch { }
        }
        /// <summary>
        /// Обновить значение температур
        /// </summary>
        public void UpdateTemperature(byte[] temp, int literCount)
        {
            try
            {
                var _newTemp = new short[temp.Length - literCount];
                for (int i = 0; i < literCount; i++)
                {
                    _newTemp[i * 4] = Convert.ToInt16(temp[i * 5 + 1]);
                    _newTemp[i * 4 + 1] = Convert.ToInt16(temp[i * 5 + 2]);
                    _newTemp[i * 4 + 2] = Convert.ToInt16(temp[i * 5 + 3]);
                    _newTemp[i * 4 + 3] = Convert.ToInt16(temp[i * 5 + 4]);
                }
                ((GlobalState)paramAmplifire.DataContext).TempCollection = new ObservableCollection<FullParam>(((GlobalState)paramAmplifire.DataContext).FullParams);
                for (int i = 0; i < literCount; i++)
                {
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Temp1State = _newTemp[i * 4];
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Temp2State = _newTemp[i * 4 + 1];
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Temp3State = _newTemp[i * 4 + 2];
                    ((GlobalState)paramAmplifire.DataContext).TempCollection[i].Temp4State = _newTemp[i * 4 + 3];

                    ((GlobalState)paramAmplifire.DataContext).FullParams.RemoveAt(i);
                    ((GlobalState)paramAmplifire.DataContext).FullParams.Insert(i, ((GlobalState)paramAmplifire.DataContext).TempCollection[i]);
                }

            }
            catch { }
        }


        private void DataGridparamAmplifire_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows(paramAmplifire.Items.Count);
        }

        private void AddEmptyRows()
        {
            try
            {
                paramAmplifire.Items.Refresh();
                int сountRowsAll = paramAmplifire.Items.Count; // количество имеющихся строк в таблице
                int maxRows = numOfLetters;  
                for (int i = 0; i < maxRows - сountRowsAll; i++)
                {
                    ((GlobalState)paramAmplifire.DataContext).FullParams.Add(new FullParam() { letter = i + 1 });
                }
            }
            catch { }
        }

        private void AddEmptyRows(int pos)
        {
            try
            {
                paramAmplifire.Items.Refresh();
                int сountRowsAll = paramAmplifire.Items.Count; // количество имеющихся строк в таблице
                int maxRows = numOfLetters;
                for (int i = pos; i < maxRows; i++)
                {
                    ((GlobalState)paramAmplifire.DataContext).FullParams.Add(new FullParam() { letter = i + 1 });
                }
            }
            catch { }
        }
    }
}
