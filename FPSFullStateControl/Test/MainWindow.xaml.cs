﻿using System.Windows;

namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Fps.numOfLetters = 10;
            var current = new byte[50] { 1, 25, 25, 0, 0, 2, 19, 22, 0, 0, 3, 25, 25, 0, 0, 4, 19, 22, 0, 0, 5, 22, 26, 0, 0, 6, 18, 18, 19, 21, 7, 19, 20, 21, 26, 8, 1, 5, 54, 0, 9, 246, 0, 1, 7, 10, 0, 0, 6, 0 };
            Fps.UpdateCurrent(current, 10);
        }


        private void ButtonPOW_Click(object sender, RoutedEventArgs e)
        {
            var power = new short[10] { 98, 99, 98, 99, 99, 88, 52, 7, 7, 3 };
            Fps.UpdatePower(power, 10);
        }

        private void ButtonPOW2_Click(object sender, RoutedEventArgs e)
        {
            var power = new short[6] { 18, 22, 150, 90, 60, 80 };
            Fps.UpdatePower(power, 6);
        }

        private void ButtonV1_Click(object sender, RoutedEventArgs e)
        {
            var voltage = new float[10] {7216, 7474, 7216, 7474, 7728, 7470, 7470, 7470, 7470, 49 };
            Fps.UpdateVoltage(voltage, 10);
        }

        private void ButtonV2_Click(object sender, RoutedEventArgs e)
        {
            var voltage = new float[10] { 0, 0, 0, 0, 0, 0,0,0,0,0 };
            Fps.UpdateVoltage(voltage, 10);
        }

        private void ButtonT1_Click(object sender, RoutedEventArgs e)
        {
            var temp = new byte[50] { 1, 49, 39, 39, 38, 2, 39, 37, 38,40, 3, 40,39,39,38,4, 39,37,38,39, 5,47,47,50,50, 6, 52, 49,49,47,7,39,40,40,41, 8, 0, 52, 0, 0, 9, 0, 52, 0, 0 , 10, 0, 30, 0, 0};
            Fps.UpdateTemperature(temp, 10);
        }

        private void ButtonC1_Click(object sender, RoutedEventArgs e)
        {
            var current = new byte[50] {1, 25, 25, 0, 0, 2, 19, 22, 0, 0,3, 25, 25, 0, 0, 4, 19, 22, 0, 0, 5, 22, 26, 0, 0, 6, 18, 18, 19, 21, 7, 19, 20,21,26, 8, 1 ,5,54,0, 9, 246, 138, 1, 7, 10, 0, 0, 6, 0 };
            Fps.UpdateCurrent(current, 10);
        }

        private void ButtonT2_Click(object sender, RoutedEventArgs e)
        {
            var temp = new byte[25] { 1,50, 30, 220, 110,2, 60, 249, 11, 15, 3, 36, 78, 22, 90, 4, 43, 66, 78, 22, 5, 40, 254, 34, 65 };
            Fps.UpdateTemperature(temp, 5);
        }

        private void ButtonC2_Click(object sender, RoutedEventArgs e)
        {
            var current = new byte[25] { 1, 20, 12, 22, 10, 2, 13, 36, 11, 15, 3, 36, 78, 22, 90, 4, 19, 6, 7, 22, 5, 40, 4, 30, 4 };
            Fps.UpdateCurrent(current,5);
        }

        private void ButtonO2_Click(object sender, RoutedEventArgs e)
        {
            
            var byteErrors = new byte[3] { 132, 40, 30 };
            Fps.UpdateError(byteErrors, 10);
        }

        private void ButtonO1_Click(object sender, RoutedEventArgs e)
        {
            var byteErrors = new byte[3] { 255, 255, 15 };
            Fps.UpdateError(byteErrors, 10);
        }
    }
}
