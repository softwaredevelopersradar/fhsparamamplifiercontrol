﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace FHSValuesConverter
{
    public class VisibilityRule : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility result = Visibility.Visible;
            if (value == null || value.ToString() == "0")
            {
                result = Visibility.Hidden;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
