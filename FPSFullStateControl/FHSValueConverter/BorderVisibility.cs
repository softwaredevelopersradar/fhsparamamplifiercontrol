﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;
namespace FHSValuesConverter
{
    [ValueConversion(typeof(bool), typeof(Thickness))]
    public class BorderVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Thickness thick;

            switch (value)
            {
                case true:
                    return thick = new Thickness(1);
                default:
                    return thick = new Thickness(0);
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}