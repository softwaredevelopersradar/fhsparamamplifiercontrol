﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FHSValuesConverter
{
    [ValueConversion(typeof(byte), typeof(string))]
    public class LoadConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte input = System.Convert.ToByte(value);
            string typeOfLoad;
            switch (input)
            {
                case 0:
                    typeOfLoad = "EQV";
                    break;

                case 1:
                    typeOfLoad = "ANT";
                    break;

                default:
                    typeOfLoad = "";
                    break;

            }
            return typeOfLoad;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
