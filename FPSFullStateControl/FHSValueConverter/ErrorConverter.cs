﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FHSValuesConverter
{
    [ValueConversion(typeof(byte), typeof(Uri))]
    public class ErrorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Uri uri;
            byte input = System.Convert.ToByte(value);
            switch (input)
            {

                case 1:
                    uri = new Uri(@"pack://application:,,,/" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                + ";component/"
                                + "Resources/red.png", UriKind.Absolute);
                    break;
                default:
                    uri = null;
                    break;

            }
            return uri;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
