﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FHSValuesConverter
{
    public class CurrentConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                var letter = (int)value[1];
                var result = (float)value[0];

                if (letter == 8 || letter == 9)
                {
                    result = result / 10;
                }

                return result.ToString();
            }
            catch 
            {
                return 0.ToString();
            }
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
