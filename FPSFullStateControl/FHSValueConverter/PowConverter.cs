﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FHSValuesConverter
{
    [ValueConversion(typeof(short), typeof(int))]
    public class PowConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            short input = System.Convert.ToByte(value);
            int result;
            return result = input * 10;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
